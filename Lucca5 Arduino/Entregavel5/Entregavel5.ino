// Lucca Gandra Entregável-5
// Capacitação Arduino


//Legenda:

//Motor1: (6,7)
//Motor2: (8,9)

//Bateria1: (A1)
//Bateria2: (A2)
//Bateria3: (A3)
//Bateria4: (A4)

// 

int bateria1,bateria2,bateria3,bateria4, bateria;
int PWM = 0;
int acao;

//---------------------------------------------------------------------------------------------------------------------------------//

void vidautil()
{
//Da pilha alcalina: nominal: 1,5V, cut off voltage: 0,9V
//Do arduino: 5V == 1023 no analogRead. Logo, 1,5V == 307 e 0,9V == 184
//Vida útil: do 307 até 184 (analogRead).

bateria1 = analogRead(A1);
bateria2 = analogRead(A2);
bateria3 = analogRead(A3);
bateria4 = analogRead(A4);

bateria1 = map(bateria1,184,307,0,25); // A pilha só dá valores de 184 até 307; e cada pilha corresponde a 25% da carga do carro
bateria2 = map(bateria2,184,307,0,25);
bateria3 = map(bateria3,184,307,0,25);
bateria4 = map(bateria4,184,307,0,25);

bateria = bateria1 + bateria2 + bateria3 + bateria4;
}

//---------------------------------------------------------------------------------------------------------------------------------//

void frente()
{
digitalWrite(6,PWM); // Motor1 Horário                                 
digitalWrite(7,LOW);

digitalWrite(8,PWM); // Motor2 Horário                                  
digitalWrite(9,LOW);
}

//---------------------------------------------------------------------------------------------------------------------------------//

void tras()
{
digitalWrite(6,LOW); // Motor1 Anti-Horário                                 
digitalWrite(7,PWM);

digitalWrite(8,LOW); // Motor2 Anti-Horário                                  
digitalWrite(9,PWM);
}

//---------------------------------------------------------------------------------------------------------------------------------//

void direita()
{
digitalWrite(6,PWM); // Motor1 Horário                                 
digitalWrite(7,LOW);

digitalWrite(8,LOW);  // Motor2 Anti-Horário                                  
digitalWrite(9,PWM);
}

//---------------------------------------------------------------------------------------------------------------------------------//

void esquerda()
{
digitalWrite(6,LOW);  // Motor1 Anti-Horário                                 
digitalWrite(7,PWM);

digitalWrite(8,PWM); // Motor2 Horário                                  
digitalWrite(9,LOW);
}

//---------------------------------------------------------------------------------------------------------------------------------//

void parada()
{
digitalWrite(6,LOW);  // Motor1 Anti-Horário                                 
digitalWrite(7,LOW);

digitalWrite(8,LOW);  // Motor2 Horário                                  
digitalWrite(9,LOW);
}

//---------------------------------------------------------------------------------------------------------------------------------//

void leituraSerial()
{
  
Serial.print("Determine a velocidade do carrinho, de 1 a 10: \n");
  
if(PWM == 0 && Serial.available() > 0) // Velocidade inicial
{
PWM = Serial.read();
PWM = map(PWM, 1, 10, 1, 255); //Velocidade PWM redefinida para parâmetros utilizáveis
Serial.print("A velocidade é: \n");
Serial.print(PWM);
}

Serial.print("8 = frente, 2 = tras, 6 = direita, 4 = esquerda, 5 = parada \n ");
Serial.print("Dica: use o teclado numérico");


if(PWM != 0 && Serial.available() > 0)
{
acao = Serial.read();
}

}

//---------------------------------------------------------------------------------------------------------------------------------//

void interpretacaoSerial()
{

if(acao == 8) frente();
if(acao == 2) tras();
if(acao == 6) direita();
if(acao == 4) esquerda();
if(acao == 5) parada();

}

//---------------------------------------------------------------------------------------------------------------------------------//

void setup() 
{
Serial.begin(9600);
  
//Indicação da saída dos OUTPUT MOTORES
pinMode(3,OUTPUT); // PWM pin

pinMode(6,OUTPUT); // I/O pin    Motor1
pinMode(7,OUTPUT); // I/O pin    Motor1

pinMode(8,OUTPUT); // I/O pin    Motor2
pinMode(9,OUTPUT); // I/O pin    Motor2

//Rotação inicial dos OUTPUT MOTORES
digitalWrite(6,LOW);                                    //MOTORES parada
digitalWrite(7,LOW);

digitalWrite(8,LOW);                                    //MOTORES parada
digitalWrite(9,LOW);

}

//---------------------------------------------------------------------------------------------------------------------------------//

void loop() 
{
vidautil();
leituraSerial();
interpretacaoSerial();

if(bateria < 5)
{
Serial.print("Sem bateria");
parada();
}

}

//---------------------------------------------------------------------------------------------------------------------------------//
